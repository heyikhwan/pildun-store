<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Gold D. Roger',
            'username' => 'admin',
            'email' => 'admin@pildun.com',
            'password' => bcrypt('adminadmin')
        ])->assignRole('admin');

        User::create([
            'name' => 'Uzumaki Adam',
            'username' => 'user',
            'email' => 'user@pildun.com',
            'password' => bcrypt('useruser')
        ])->assignRole('user');
    }
}
