<?php

namespace Database\Seeders;

use App\Models\TransaksiDetail;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TransaksiDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransaksiDetail::create([
            'product_id' => 1,
            'transaksi_id' => 1,
            'qty' => 2,
            'price' => 100000,
            'sub_total' => 200000,
        ]);
        TransaksiDetail::create([
            'product_id' => 2,
            'transaksi_id' => 1,
            'qty' => 2,
            'price' => 100000,
            'sub_total' => 200000,
        ]);
        TransaksiDetail::create([
            'product_id' => 2,
            'transaksi_id' => 2,
            'qty' => 2,
            'price' => 100000,
            'sub_total' => 200000,
        ]);
    }
}
