<?php

namespace Database\Seeders;

use App\Models\ProductGallery;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductGallery::create([
            'product_id' => 1,
            'name' => 'banner-2.jpg',
            'image' => '1060491986-banner-2.jpg'

        ]);
    }
}
