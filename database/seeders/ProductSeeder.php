<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'category_id' => 1,
            'name' => 'Baju Pildun',
            'slug' => 'baju-pildun',
            'description' => 'Baju Mantap',
            'price' => 100000,
            'stock' => 50,
            'weight' => 50,
        ]);
        Product::create([
            'category_id' => 1,
            'name' => 'Baju Brazil',
            'slug' => 'baju-brazil',
            'description' => 'Baju Mantap',
            'price' => 100000,
            'stock' => 50,
            'weight' => 50,
        ]);
        Product::create([
            'category_id' => 1,
            'name' => 'Baju Portugal',
            'slug' => 'baju-portugal',
            'description' => 'Baju Mantap',
            'price' => 100000,
            'stock' => 50,
            'weight' => 50,
        ]);
    }
}
