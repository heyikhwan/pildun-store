<?php

namespace Database\Seeders;

use App\Models\Transaksi;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaksi::create([
            'user_id' => 2,
            'no_invoice' => 'INV/2',
            'status' => 1,
            'total_harga' => 400000,
        ]);
        Transaksi::create([
            'user_id' => 1,
            'no_invoice' => 'INV/1',
            'status' => 1,
            'total_harga' => 200000,
        ]);
    }
}
