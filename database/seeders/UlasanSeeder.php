<?php

namespace Database\Seeders;

use App\Models\Review;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UlasanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Review::create([
            'user_id' => 2,
            'product_id' => 1,
            'rating' => 5,
            'ulasan' => 'pengemasan bagus, pengiriman cepat, produknya ori & harganya di diskon. Aseek',
        ]);
        Review::create([
            'user_id' => 1,
            'product_id' => 1,
            'rating' => 3,
            'ulasan' => 'cacat produk',
        ]);
        Review::create([
            'user_id' => 2,
            'product_id' => 1,
            'rating' => 1,
            'ulasan' => 'gblk',
        ]);
        Review::create([
            'user_id' => 2,
            'product_id' => 1,
            'rating' => 1,
            'ulasan' => 'gblk',
        ]);
        Review::create([
            'user_id' => 2,
            'product_id' => 1,
            'rating' => 1,
            'ulasan' => 'gblk',
        ]);
        Review::create([
            'user_id' => 2,
            'product_id' => 1,
            'rating' => 1,
            'ulasan' => 'gblk',
        ]);
        Review::create([
            'user_id' => 2,
            'product_id' => 1,
            'rating' => 1,
            'ulasan' => 'gblk',
        ]);
    }
}
