<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->string('no_invoice')->unique()->nullable();
            $table->integer('status')->default(0)->comment('0: masih di keranjang | 1: sedang diproses | 2: dikirim | 3: diterima | 4: dibatalkan');
            $table->integer('total_harga')->default(0);
            $table->text('note')->nullable();
            $table->boolean('is_review')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
};
