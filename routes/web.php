<?php

use App\Http\Controllers\Backend\CategoryController as BackendCategoryController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ProductController as BackendProductController;
use App\Http\Controllers\Backend\TransaksiController as BackendTransaksiController;
use App\Http\Controllers\Backend\BannerController as BackendBannerController;
use App\Http\Controllers\Backend\ReviewController as BackendReviewController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\AccountController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CategoryController;
use App\Http\Controllers\Frontend\HistoryController;
use App\Http\Controllers\Frontend\HomeController;


Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::resource('kategori', CategoryController::class)->only('show');
Route::get('produk/cari', [ProductController::class, 'search'])->name('produk.cari');
Route::resource('produk', ProductController::class)->only(['index', 'show']);

Route::middleware('auth')->group(function() {
    Route::post('keranjang/{cart}', [CartController::class, 'cart'])->name('keranjang.store');
Route::get('keranjang', [CartController::class, 'index'])->name('keranjang.index');
Route::put('keranjang/{cart}/update', [CartController::class, 'update'])->name('keranjang.update');
Route::delete('keranjang/{cart}/delete', [CartController::class, 'delete'])->name('keranjang.delete');
Route::get('keranjang/checkout', [CartController::class, 'checkout'])->name('keranjang.checkout');
Route::put('payment/{payment}', [CartController::class, 'payment'])->name('keranjang.payment');
Route::get('payment/success', function() {
    return view('frontend.pages.keranjang.payment-success');
})->name('payment.success');

Route::post('review', [HistoryController::class, 'review'])->name('review.store');
Route::resource('riwayat-pesanan', HistoryController::class)->only(['index', 'update']);

Route::resource('akun', AccountController::class)->only(['index', 'update']);
});

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:admin'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('user', UserController::class)->except('show');
    Route::get('user/data', [UserController::class, 'data'])->name('user.data');

    Route::resource('kategori', BackendCategoryController::class)->except('show');
    Route::get('kategori/data', [BackendCategoryController::class, 'data'])->name('kategori.data');

    Route::resource('produk', BackendProductController::class)->except('show');
    Route::get('produk/data', [BackendProductController::class, 'data'])->name('produk.data');
    Route::put('produk/foto/hapus/{id}', [BackendProductController::class, 'imgDestroy']);

    Route::resource('transaksi', BackendTransaksiController::class)->only('index');
    Route::get('transaksi/data', [BackendTransaksiController::class, 'data'])->name('transaksi.data');
    Route::put('transaksi/updateOrder/{id}', [BackendTransaksiController::class, 'updateOrder'])->name('transaksi.updateOrder');
    Route::put('transaksi/completeOrder/{id}', [BackendTransaksiController::class, 'completeOrder'])->name('transaksi.completeOrder');
    Route::put('transaksi/cancelOrder/{id}', [BackendTransaksiController::class, 'cancelOrder'])->name('transaksi.cancelOrder');

    Route::resource('banner', BackendBannerController::class)->except('show');
    Route::get('banner/data', [BackendBannerController::class, 'data'])->name('banner.data');

    Route::resource('ulasan', BackendReviewController::class)->except('show');
    Route::get('ulasan/data', [BackendReviewController::class, 'data'])->name('ulasan.data');
});
