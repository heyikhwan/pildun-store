@extends('layouts.backend')
@section('title', 'Edit Produk')

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="javascript: void(0);">Produk</a>
</li>
<li class="breadcrumb-item active">Edit Produk</li>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{ route('admin.produk.update', $product->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-12 mb-3">
                    <label for="name" class="form-label">Nama Produk<sup class="text-danger">*</sup></label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror"
                        value="{{ old('name') ?? $product->name }}" required>

                    @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <label for="category">Kategori<sup class="text-danger">*</sup></label>
                    <select class="form-select @error('category') is-invalid @enderror" name="category" required>
                        <option disabled selected>--- Pilih Kategori ---</option>
                        @foreach ($categories as $item)
                        <option value="{{ $item->id }}" @selected(old('role')==$item->id || $product->category_id ==
                            $item->id)>{{ $item->name }}</option>
                        @endforeach
                    </select>

                    @error('category')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-5 mb-3">
                    <label for="price" class="form-label">Harga<sup class="text-danger">*</sup></label>
                    <input type="text" name="price" class="form-control @error('price') is-invalid @enderror"
                        value="{{ old('price') ?? $product->price }}" required>

                    @error('price')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-3 mb-3">
                    <label for="stock" class="form-label">Stok<sup class="text-danger">*</sup></label>
                    <input type="text" name="stock" class="form-control @error('stock') is-invalid @enderror"
                        value="{{ old('stock') ?? $product->stock }}" required>

                    @error('stock')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-4 mb-3">
                    <label for="weight" class="form-label">Berat (gram)<sup class="text-danger">*</sup></label>
                    <input type="text" name="weight" class="form-control @error('weight') is-invalid @enderror"
                        value="{{ old('weight') ?? $product->weight }}" required>

                    @error('weight')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Deskripsi Produk<sup class="text-danger">*</sup></label>
                    <textarea class="form-control @error('description') is-invalid @enderror" name="description"
                        id="description" cols="30" rows="10"
                        required>{{ old('description') ?? $product->description }}</textarea>

                    @error('description')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <label for="foto">Foto Produk</label>
                    @if (!$product->galleries->isEmpty())
                    <div class="my-2 alert alert-info" id="info-foto">
                        <span>Foto produk yang sudah di input: </span>
                        <div class="d-flex flex-wrap gap-2 my-2">
                            @foreach ($product->galleries as $foto)
                            <a href="{{ url('storage/product', $foto->image) }}" data-fancybox="gallery2"
                                data-caption="{{ $foto->name }}">
                                <div class="position-relative">
                                    <img src="{{ url('storage/product', $foto->image) }}" alt="{{ $foto->name }}"
                                        width="80" class="img-fluid img-thumbnail">

                                    <div class="position-absolute" style="top: -10px; right: -5px;"
                                        id="foto-{{ $foto->id }}"
                                        onclick="event.preventDefault();imgDestroy({{ $foto->id }});">
                                        <i class="ri-close-circle-fill text-danger fs-4"></i>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                        </div>
                        <p class="m-0">Tekan tombol silang untuk menghapus foto.</p>
                        <p class="m-0">Upload foto jika ingin menambahkan foto produk.</p>
                    </div>
                    @endif
                    <input type="file" class="filepond filepond-input-multiple" multiple id="foto" name="foto[]"
                        data-allow-reorder="true" data-max-file-size="2MB" accept="image/*">
                    <small>Maksimum ukuran file adalah 2MB</small>
                </div>
            </div>
            <div class="grid g-3 float-end">
                <a href="{{ route('admin.produk.index') }}" class="btn btn-light">Kembali</a>
                <button type="submit" class="btn btn-primary">Ubah</button>
            </div>
    </div>

    </form>
</div>
</div>

@endsection

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"
    type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('assets/libs/filepond/filepond.min.css') }}" type="text/css" />
<link rel="stylesheet"
    href="{{ URL::asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
<link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('scripts')
<!--jquery cdn-->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ URL::asset('assets/libs/filepond/filepond.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.js') }}">
</script>
<script
    src="{{ URL::asset('assets/libs/filepond-plugin-file-validate-size/filepond-plugin-file-validate-size.min.js') }}">
</script>
<script
    src="{{ URL::asset('assets/libs/filepond-plugin-image-exif-orientation/filepond-plugin-image-exif-orientation.min.js') }}">
</script>
<script src="{{ URL::asset('assets/libs/filepond-plugin-file-encode/filepond-plugin-file-encode.min.js') }}"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>

<script>
    // FilePond
FilePond.registerPlugin(
    // encodes the file as base64 data
    FilePondPluginFileEncode,
    // validates the size of the file
    FilePondPluginFileValidateSize,
    // corrects mobile image orientation
    FilePondPluginImageExifOrientation,
    // previews dropped images
    FilePondPluginImagePreview,
    FilePondPluginFileValidateType
);

const inputMultipleElements = document.querySelectorAll('#foto');

if(inputMultipleElements){
// loop over input elements
Array.from(inputMultipleElements).forEach(function (inputElement) {
    // create a FilePond instance at the input element location
    FilePond.create(inputElement), {
        acceptedFileTypes: ['image/*'],
    };
})
}

</script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>

<!-- Delete Image -->
<script src="{{ URL::asset('/assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
    let jmlFoto = {{ $product->galleries->count() }}

    function imgDestroy(id) {
        let el = event.target.parentElement.parentElement.parentElement;

        Swal.fire({
            html: '<div class="mt-3">' +
                '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
                '<div class="mt-4 pt-2 fs-15 mx-5">' +
                '<h4>Anda Yakin ?</h4>' +
                '<p class="text-muted mx-4 mb-0">Anda yakin ingin menghapus foto ini ?</p>' +
                '</div>' +
                '</div>',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonClass: 'btn btn-primary w-xs mb-1',
            confirmButtonText: 'Ya, Hapus!',
            cancelButtonClass: 'btn btn-danger w-xs mb-1 me-2',
            cancelButtonText: 'Batal',
            buttonsStyling: false,
            showCloseButton: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/admin/produk/foto/hapus/' + id,
                    method: 'put',
                    context: el,
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(data) {
                        Toastify({
                            text: "Foto berhasil dihapus",
                            duration: 5000,
                            close: true,
                            gravity: "top", // `top` or `bottom`
                            position: "right", // `left`, `center` or `right`
                            stopOnFocus: true, // Prevents dismissing of toast on hover
                            className: 'bg-primary',
                            style: {
                                background: "linear-gradient(to right, rgb(10, 179, 156), rgb(64, 81, 137))"
                            },
                            onClick: function() {} // Callback after click
                        }).showToast();

                        jmlFoto--;
                        el.remove();

                        jmlFoto == 0 ? hideInfoFoto() : '';
                    }
                })
            }
        })
    }

    const hideInfoFoto = () => {
        return $('#info-foto').addClass('d-none');
    }
</script>
@endpush