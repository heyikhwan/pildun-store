@extends('layouts.backend')
@section('title', 'Tambah Produk')

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="javascript: void(0);">Produk</a>
</li>
<li class="breadcrumb-item active">Tambah Produk</li>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{ route('admin.produk.store') }}" method="post">
            @csrf

            <div class="row">
                <div class="col-12 mb-3">
                    <label for="name" class="form-label">Nama Produk<sup class="text-danger">*</sup></label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror"
                        value="{{ old('name') }}" required>

                    @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <label for="category">Kategori<sup class="text-danger">*</sup></label>
                    <select class="form-select @error('category') is-invalid @enderror" name="category" required
                        data-choices>
                        <option disabled selected>--- Pilih Kategori ---</option>
                        @foreach ($categories as $item)
                        <option value="{{ $item->id }}" @selected(old('category')==$item->id)>{{ $item->name }}
                        </option>
                        @endforeach
                    </select>

                    @error('category')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-5 mb-3">
                    <label for="price" class="form-label">Harga<sup class="text-danger">*</sup></label>
                    <input type="number" name="price" class="form-control @error('price') is-invalid @enderror"
                        value="{{ old('price') }}" required>

                    @error('price')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-3 mb-3">
                    <label for="stock" class="form-label">Stok<sup class="text-danger">*</sup></label>
                    <input type="stock" name="stock" class="form-control @error('stock') is-invalid @enderror"
                        value="{{ old('stock') }}" required>

                    @error('stock')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-4 mb-3">
                    <label for="weight" class="form-label">Berat (gram)<sup class="text-danger">*</sup></label>
                    <input type="weight" name="weight" class="form-control @error('weight') is-invalid @enderror"
                        value="{{ old('weight') }}" required>

                    @error('weight')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <label for="description" class="form-label">Deskripsi Produk</label>
                    <textarea class="form-control @error('description') is-invalid @enderror" name="description"
                        id="description" cols="30" rows="10" required>{{ old('description') }}</textarea>

                    @error('description')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <label for="foto">Foto Produk<sup class="text-danger">*</sup></label>

                    <input type="file" class="filepond filepond-input-multiple" multiple id="foto" name="foto[]"
                        data-allow-reorder="true" data-max-file-size="1MB" accept="image/*">
                    <small>Maksimum ukuran file adalah 1MB</small>
                </div>
            </div>
            <div class="grid g-3 float-end">
                <a href="{{ route('admin.produk.index') }}" class="btn btn-light">Kembali</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </div>

    </form>
</div>
</div>

@endsection

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"
    type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('assets/libs/filepond/filepond.min.css') }}" type="text/css" />
<link rel="stylesheet"
    href="{{ URL::asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
<link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/libs/filepond/filepond.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.js') }}">
</script>
<script
    src="{{ URL::asset('assets/libs/filepond-plugin-file-validate-size/filepond-plugin-file-validate-size.min.js') }}">
</script>
<script
    src="{{ URL::asset('assets/libs/filepond-plugin-image-exif-orientation/filepond-plugin-image-exif-orientation.min.js') }}">
</script>
<script src="{{ URL::asset('assets/libs/filepond-plugin-file-encode/filepond-plugin-file-encode.min.js') }}"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>

<script>
    // FilePond
FilePond.registerPlugin(
    // encodes the file as base64 data
    FilePondPluginFileEncode,
    // validates the size of the file
    FilePondPluginFileValidateSize,
    // corrects mobile image orientation
    FilePondPluginImageExifOrientation,
    // previews dropped images
    FilePondPluginImagePreview,
    FilePondPluginFileValidateType
);

const inputMultipleElements = document.querySelectorAll('#foto');

if(inputMultipleElements){
    // loop over input elements
    Array.from(inputMultipleElements).forEach(function (inputElement) {
        // create a FilePond instance at the input element location
        FilePond.create(inputElement), {
            acceptedFileTypes: ['image/*'],
        };
    })
}
</script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>

<!-- Delete Image -->
<script src="{{ URL::asset('/assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
@endpush