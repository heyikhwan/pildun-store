@extends('layouts.backend')
@section('title', 'Banner')
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="javascript: void(0);">Banner</a>
</li>
<li class="breadcrumb-item active">Tambah Banner</li>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.banner.store') }}" method="post">
                    @csrf

                    <div class="mb-3">
                        <label for="image" class="form-label">Image<sup class="text-danger">*</sup></label>
                        <input type="file" class="filepond" id="image" name="image" data-max-file-size="2MB"
                            accept="image/*" required>
                        <small class="text-black-50">Maksimum ukuran file adalah 2MB</small>
                        
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="d-flex justify-content-end mt-3 gap-2">
                        <a href="{{ route('admin.banner.index') }}" class="btn btn-light">Kembali</a>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ URL::asset('assets/libs/filepond/filepond.min.css') }}" type="text/css" />
<link rel="stylesheet"
    href="{{ URL::asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.css') }}">
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/libs/filepond/filepond.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.js') }}">
</script>
<script
    src="{{ URL::asset('assets/libs/filepond-plugin-file-validate-size/filepond-plugin-file-validate-size.min.js') }}">
</script>
<script
    src="{{ URL::asset('assets/libs/filepond-plugin-image-exif-orientation/filepond-plugin-image-exif-orientation.min.js') }}">
</script>
<script src="{{ URL::asset('assets/libs/filepond-plugin-file-encode/filepond-plugin-file-encode.min.js') }}"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>

<script>
    // FilePond
FilePond.registerPlugin(
    // encodes the file as base64 data
    FilePondPluginFileEncode,
    // validates the size of the file
    FilePondPluginFileValidateSize,
    // corrects mobile image orientation
    FilePondPluginImageExifOrientation,
    // previews dropped images
    FilePondPluginImagePreview,
    FilePondPluginFileValidateType
);

FilePond.create(document.querySelector('#image'));
</script>
@endpush
