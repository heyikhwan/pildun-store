@extends('layouts.backend')
@section('title', 'Kategori')
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="javascript: void(0);">Kategori</a>
</li>
<li class="breadcrumb-item active">List Kategori</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="d-grid justify-content-end">
            <a href="{{ route('admin.kategori.create') }}" class="btn btn-primary brn waves-effect waves-light">Tambah
                Kategori
            </a>
        </div>
    </div>

    <div class="card-body">
        <table id="datatable" class="table data nowrap dt-responsive align-middle table-hover table-bordered"
            style="width:100%">
            <thead>
                <tr>
                    <th class="text-center" style="width: 25px">#</th>
                    <th>Nama</th>
                    <th>Icon</th>
                    <th style="width:7%;">Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('styles')
<!--datatable css-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
<!--datatable responsive css-->
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
<!-- Sweet Alert css-->
<link href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
@endpush

@push('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!--datatable js-->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>

<script>
    $(document).ready(function() {
            let table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                order: [
                    [1, "asc"]
                ],
                ajax: {
                    url: '{{ route('admin.kategori.data') }}',
                },
                columns: [
                    {
                        data: null,
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'icon',
                        name: 'icon'
                    },
                    {
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false,
                    },
                ]
            });
        });
</script>
<!-- Sweet Alerts js -->
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

{{-- Delete Data Sweet Alert --}}
<script>
    const confirmDelete = id => {
            Swal.fire({
                html: '<div class="mt-3">' +
                    '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
                    '<div class="mt-4 pt-2 fs-15 mx-5">' +
                    '<h4>Anda Yakin ?</h4>' +
                    '<p class="text-muted mx-4 mb-0">Anda yakin ingin menghapus data ini ?</p>' +
                    '</div>' +
                    '</div>',
                showCancelButton: true,
                reverseButtons: true,
                confirmButtonClass: 'btn btn-primary w-xs mb-1',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonClass: 'btn btn-danger w-xs mb-1 me-2',
                cancelButtonText: 'Batal',
                buttonsStyling: false,
                showCloseButton: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#delete-${id}`).submit();
                    Swal.fire(
                        'Dihapus!', 'Data berhasil dihapus.', 'success'
                    )
                }
            })
        }
</script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
@endpush