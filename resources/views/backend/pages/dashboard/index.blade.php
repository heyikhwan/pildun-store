@extends('layouts.backend')
@section('title', 'Dashboard')
@push('styles')
   
@endpush
@section('content')
<div class="h-100">
    <div class="row mb-3 pb-1">
        <div class="col-12">
            <div class="d-flex align-items-lg-center flex-lg-row flex-column">
                <div class="flex-grow-1">
                    <h4 class="fs-16 mb-1">Selamat Datang, {{ Auth::user()->name }}!</h4>
                    <h5 class="text-muted mb-0"></h5>
                </div>
            </div><!-- end card header -->
        </div>
        <!--end col-->
    </div>
    <!--end row-->

    <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-animate">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Pendapatan </p>
                        </div>
                    </div>
                    <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-4">Rp.<span class="counter-value" data-target="{{ $pendapatan }}">{{ $pendapatan }}</span></h4>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-success rounded fs-3">
                                <i class="bx bx-dollar-circle text-success"></i>
                            </span>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-animate">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                         <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Pesanan Sukses</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value" data-target="{{ $pesanan }}">{{ $pesanan }}</span></h4>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-info rounded fs-3">
                                <i class="bx bx-shopping-bag text-info"></i>
                            </span>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-animate">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-medium text-muted text-truncate mb-0">User</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value" data-target="{{ $customer }}">{{ $customer }}</span></h4>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-warning rounded fs-3">
                                <i class="bx bx-user-circle text-warning"></i>
                            </span>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-animate">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Produk</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value" data-target="{{ $product }}">{{ $product }}</span></h4>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-primary rounded fs-3">
                                <i class="bx bx-wallet text-primary"></i>
                            </span>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div> <!-- end row-->

    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-header border-0 align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Pendapatan</h4>
                    <div>
                        <button type="button" class="btn btn-secondary btn-sm">
                            1Y
                        </button>
                    </div>
                </div><!-- end card header -->

                <div class="card-header p-0 border-0 bg-soft-light">
                    <div class="row g-0 text-center">
                        <div class="col-6 col-sm-6">
                            <div class="p-3 border border-dashed border-start-0">
                                <h5 class="mb-1"><span class="counter-value" data-target="{{ $pesanan }}">{{ $pesanan }}</span></h5>
                                <p class="text-muted mb-0">Pesanan</p>
                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-6 col-sm-6">
                            <div class="p-3 border border-dashed border-start-0">
                                <h5 class="mb-1">Rp.<span class="counter-value" data-target="{{ $pendapatan }}">{{ $pendapatan }}</span></h5>
                                <p class="text-muted mb-0">Pendapatan</p>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                </div><!-- end card header -->

                <div class="card-body">
                    <div class="w-100">
                        <div id="chartKeuangan" data-colors='["--vz-primary", "--vz-success", "--vz-danger"]' class="apex-charts" dir="ltr"></div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->
        
        <div class="col-xl-6">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">5 Pesanan Terbaru</h4>
                </div><!-- end card header -->

                <div class="card-body">
                    <div class="table-responsive table-card">
                        <table class="table table-borderless table-centered align-middle table-nowrap mb-0">
                            <thead class="text-muted table-light">
                                <tr>
                                    <th style="width:15%;" scope="col">No. Invoice</th>
                                    <th scope="col">Pembeli</th>
                                    <th scope="col">Total Harga</th>
                                    <th style="width:10%;" scope="col">Status</th>
                                    <th style="width:10%;" scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pesanan_terbaru as $data)
                                <tr>
                                    <td>{{ $data->no_invoice }}</td>
                                    <td>{{ $data->user->name }}</td>
                                    <td>
                                        <span class="text-success">Rp. {{ number_format($data->total_harga, 0, ',', '.') }}</span>
                                    </td>
                                    <td>
                                        @if ($data->status == 1)
                                        <span class="badge bg-info">Sedang diproses</span>
                                        @elseif ($data->status == 2)
                                        <span class="badge bg-warning">Dikirim</span>
                                        @elseif ($data->status == 3)
                                        <span class="badge bg-success">Diterima</span>
                                        @elseif ($data->status == 4)
                                        <span class="badge bg-danger">Dibatalkan</span>
                                        @endif
                                    </td>
                                    <td>
                                        <button data-bs-toggle="modal" data-bs-target="#modalDetail-{{ $data->id }}" class="btn btn-sm btn-soft-info me-1">Lihat Detail</button>
                                    </td>
                                </tr><!-- end tr -->                                   
                                @endforeach
                            </tbody><!-- end tbody -->
                        </table><!-- end table -->
                    </div>
                </div>
            </div> <!-- .card-->
        </div> <!-- .col-->
</div> <!-- end .h-100-->
@foreach ($pesanan_terbaru as $item)
    <!-- Default Modals -->
<div id="modalDetail-{{ $item->id }}" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Detail Transaksi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
            </div>
            <div class="modal-body">
                <h5>No. Invoice : {{ $item->no_invoice }}</h5>
                <table id=""
                class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Produk</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Sub Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($item->detail->where('transaksi_id', $item->id) as $detail)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $detail->product->name }}</td>
                        <td>{{ $detail->qty }}</td>
                        <td class="text-end">Rp. {{ $detail->product->price }}</td>
                        <td class="text-end">Rp. {{ $detail->sub_total }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="4" class="text-end">Total Harga : </td>
                        <td class="text-end">Rp. {{ $item->total_harga }}</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
            </div>
    
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endforeach
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
        var bulan = JSON.parse('{!! json_encode($bulan) !!}');
        // Grafik Keuangan
        var pendapatan = JSON.parse('{!! json_encode($pendapatan2) !!}');
        var options = {
            series: [{
                name: 'Pendapatan',
                data: pendapatan
            }],
            chart: {
                toolbar: {
                    show: false,
                },
                type: 'area',
                height: 350,
            },
            dataLabels: {
                enabled: false
            }, 
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: bulan,
            },
            yaxis: {
                title: {
                    text: 'Rp'
                }
            },
            responsive: [{
                breakpoint: 1000,
            }],
            fill: {
                opacity: 1
            },
            title: {
    align: 'center',
    margin: 10,
    offsetX: 0,
    offsetY: 0,
    floating: false,
    style: {
      fontSize:  '14px',
      fontWeight:  'bold',
      fontFamily:  'Nunito',
      color:  '#263238'
    },
},
            tooltip: {
                y: {
                    formatter: function(val) {
                        return "Rp. " + val
                    }
                }
            }
        };

        var chart = new ApexCharts(document.querySelector("#chartKeuangan"), options);
        chart.render();
        </script>
@endpush
