<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-layout="vertical" data-topbar="light"
    data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') | Pildun Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />

    @include('backend.partials.styles')
</head>

<body>
    <div id="layout-wrapper">
        @include('backend.partials.topbar')
        @include('backend.partials.sidebar')
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- Start right Content here -->
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">@yield('title')</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        @yield('breadcrumb')
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <!-- start content -->
                    <div class="row">
                        <div class="col-12">                      
                            @yield('content')
                        </div>
                    </div>
                    <!-- end content -->
                </div>
            </div>

            @include('backend.partials.footer')
        </div>
        <!-- end main content-->
    </div>

    <!--start back-to-top-->
    <button onclick="topFunction()" class="btn btn-danger btn-icon" id="back-to-top" style="bottom: 40px">
        <i class="ri-arrow-up-line"></i>
    </button>
    <!--end back-to-top-->

    @include('backend.partials.scripts')
</body>

</html>