<form class="d-flex" action="{{ route('produk.cari') }}" method="GET">
    <input class="form-control me-2" type="search" name="search" placeholder="Cari..." aria-label="Search">
    <button class="btn btn-outline-success" type="submit"><i class="ri-search-line"></i></button>
</form>