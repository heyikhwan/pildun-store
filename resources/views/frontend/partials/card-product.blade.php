<div class="card">
    <a href="{{ route('produk.show', $item->slug) }}" class="card-img-top text-center">
        <img src="{{ url('storage/product', $item->galleries[0]->image) }}" class="img-fluid">
    </a>
    <div class="card-body">
        <a href="{{ route('produk.show', $item->slug) }}" class="text-decoration-none text-dark d-block">{{ Str::limit($item->name, 50) }}</a>
        <span class="fw-bold">Rp {{ number_format($item->price) }}</span>
    </div>
</div>