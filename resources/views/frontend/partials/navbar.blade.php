<header class="sticky-top">
    <nav class="navbar navbar-expand-lg bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                <b>Pildun</b>Store
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mt-2 mt-lg-0 mb-lg-0 d-lg-flex align-items-lg-center gap-3">
                    @auth
                    <li>
                        <a href="{{ route('keranjang.index') }}" class="text-decoration-none d-flex align-items-center gap-2 text-dark">
                            <i class="ri-shopping-cart-2-line fs-5"></i> <span>Keranjang</span><span class="text-black-50">({{ $cart_count }})</span>
                        </a>
                    </li>
                    <li class="opacity-50 d-none d-lg-block">|</li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Hai, <span class="text-black-50">{{ auth()->user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item d-flex align-items-center gap-2"
                                    href="{{ route('akun.index') }}"><i class="ri-account-circle-line"></i>Akun</a></li>
                            <li><a class="dropdown-item d-flex align-items-center gap-2" href="{{ route('riwayat-pesanan.index') }}"><i
                                        class="ri-history-line"></i>Riwayat</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="dropdown-item d-flex align-items-center gap-2"><i
                                            class="ri-logout-circle-line"></i>Keluar</button>
                                </form>
                            </li>
                        </ul>
                    </li>
                    @else
                    <a href="{{ route('login') }}" class="btn btn-outline-primary">Masuk</a>
                    <a href="{{ route('register') }}" class="btn btn-primary">Daftar</a>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</header>