@extends('layouts.app')

@section('title', 'Checkout')

@section('content')
<form action="{{ route('keranjang.payment', $transaction->id) }}" method="post">
    @csrf
    @method('put')

    <div class="row">
        @if (!is_null($transaction))
        <div class="col-12 col-lg-8">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <h5>Informasi Pengiriman</h5>
                    <hr>

                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Lengkap<sup class="text-danger">*</sup></label>
                        <input type="text" name="name" id="name"
                            class="form-control @error('name') is-invalid @enderror"
                            value="{{ old('name') ?? $user->name }}" required>
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="no_telp" class="form-label">No. Handphone<sup class="text-danger">*</sup></label>
                        <input type="text" name="no_telp" id="no_telp"
                            class="form-control @error('no_telp') is-invalid @enderror"
                            value="{{ old('no_telp') ?? $user->no_telp }}" required>
                        @error('no_telp')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="address" class="form-label">Alamat Lengkap<sup class="text-danger">*</sup></label>
                        <textarea name="address" id="address" rows="5"
                            class="form-control @error('address') is-invalid @enderror" style="resize: none"
                            required>{{ old('address') ?? $user->address }}</textarea>
                            @error('address')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="note" class="form-label">Catatan Pembelian</label>
                        <textarea name="note" id="note" rows="5"
                            class="form-control @error('note') is-invalid @enderror"
                            style="resize: none">{{ old('note') }}</textarea>
                            @error('note')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-4">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <h6>Ringkasan Belanja</h6>
                    <hr>
                    <div class="d-flex flex-column gap-3">
                        @foreach ($transaction->detail as $item)
                        <div>
                            <small class="mb-1">{{ $item->product->name }}</small>
                            <small class="text-black-50 d-flex flex-wrap">Rp {{ number_format($item->price)
                                }}
                                x {{ $item->qty }}</small>
                        </div>
                        @endforeach
                    </div>
                    <hr>
                    <div class="d-grid">
                        <div class="d-flex align-items-center justify-content-between mb-3">
                            <h5 class="m-0">Total Harga</h5>
                            <h4 class="m-0 fw-bold">Rp {{ number_format($transaction->total_harga) }}</h4>
                        </div>
                        <button type="submit" class="btn btn-primary">Bayar Sekarang</button>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="col-12">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <p class="m-0 text-center text-secondary opacity-50 fs-4 py-4">Keranjang Belanja Kosong</p>
                </div>
            </div>
        </div>

        @push('script')
        <script>
            setInterval(() => {
                    window.location = '/keranjang'
                }, 1000);
        </script>
        @endpush
        @endif
    </div>
</form>
@endsection