@extends('layouts.app')

@section('title', 'Keranjang Belanja')

@section('content')
<div class="row">
    <div class="col-12">
        <h4 class="mb-4">Keranjang Belanja</h4>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card border-0 shadow-sm">
            @if (!is_null($transaction))
            <form action="{{ route('keranjang.update', $transaction->id) }}" method="post">
                @csrf
                @method('put')

                <div class="card-body d-flex flex-column gap-2">
                    @foreach ($transaction->detail as $item)
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-2">
                                    <img src="{{ url('storage/product', $item->product->galleries[0]->image) }}"
                                        alt="Nama Produk" class="img-thumbnail img-fluid border-0">
                                </div>

                                <div class="col">
                                    <p class="mb-1">{{ $item->product->name }}</p>
                                    <h6>Rp {{ number_format($item->price) }}</h6>
                                    <div class="d-flex align-items-center gap-2 mt-3">
                                        <small class="form-label">Quantity</small>
                                        <input type="number" name="qty[]" id="qty" class="form-control form-control-sm"
                                            min="1" value="{{ $item->qty }}" max="{{ $item->product->stock }}"
                                            style="width: 60px">
                                        <small class="text-black-50">max. pembelian {{ $item->product->stock }} pcs.</small>
                                    </div>
                                    <button type="button" onclick="confirmDelete({{ $item->id }})"
                                        class="btn btn-sm btn-danger d-flex align-items-center gap-2 mt-3">
                                        <i class="ri-delete-bin-fill"></i> Hapus
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="d-grid mt-3">
                        <button type="submit" class="btn btn-primary">Beli</button>
                    </div>
                </div>
            </form>
            @else
            <div class="card-body">
                <p class="m-0 text-center text-secondary opacity-50 fs-4 py-4">Keranjang Belanja Kosong</p>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@push('style')
<!-- Sweet Alert css-->
<link href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<!-- Sweet Alerts js -->
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<!--jquery cdn-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

{{-- Delete Data Sweet Alert --}}
<script>
    const confirmDelete = id => {
            Swal.fire({
                html: '<div class="mt-3">' +
                    '<lord-image src="https://cdn.lordimage.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-image>' +
                    '<div class="mt-4 pt-2 fs-15 mx-5">' +
                    '<h4>Anda Yakin ?</h4>' +
                    '<p class="text-muted mx-4 mb-0">Anda yakin ingin menghapus item ini ?</p>' +
                    '</div>' +
                    '</div>',
                showCancelButton: true,
                reverseButtons: true,
                confirmButtonClass: 'btn btn-primary w-xs mb-1',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonClass: 'btn btn-danger w-xs mb-1 me-2',
                cancelButtonText: 'Batal',
                buttonsStyling: false,
                showCloseButton: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: 'keranjang/' + id + '/delete',
                        method: 'delete',
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function(data) {
                            location.reload();
                        }
                    })
                }
            })
        }
</script>
@endpush