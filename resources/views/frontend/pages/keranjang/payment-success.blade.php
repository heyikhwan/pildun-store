@extends('layouts.app')

@section('title', 'Pembayaran Berhasil')
    
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm">
                <div class="card-body text-center">
                    <img src="{{ asset('img/success.gif') }}" alt="Payment Success" width="30%">
                    <p class="mb-3 fw-bold fs-4 text-black-50">Pembayaran berhasil. Pesanan Anda akan segera diproses.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        setTimeout(() => {
            window.location = '/riwayat-pesanan'
        }, 3000);
    </script>
@endpush