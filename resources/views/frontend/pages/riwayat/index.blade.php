@extends('layouts.app')

@section('title', 'Riwayat Pesanan')

@section('content')
<div class="row">
    <div class="col-12">
        <h5 class="mb-4">Riwayat Pesanan</h5>
        <div class="card border-0 shadow-sm">
            <div class="card-body">
                <div class="d-flex flex-column gap-3">
                    @forelse ($histories as $items)
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="mb-3 d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center gap-2">
                                    <i class="ri-handbag-line"></i>
                                    <span class="fw-bold">Belanja</span>{{ $items->created_at->format('d F Y') }}
                                </div>

                                <div class="d-flex align-items-center gap-3">
                                    <span>No. Invoice <b>{{ $items->no_invoice }}</b></span>
                                    @if ($items->status == 1)
                                    <span class="badge bg-info">Sedang diproses</span>
                                    @elseif ($items->status == 2)
                                    <span class="badge bg-warning">Dikirim</span>
                                    @elseif ($items->status == 3)
                                    <span class="badge bg-success">Selesai</span>
                                    @elseif ($items->status == 4)
                                    <span class="badge bg-danger">Dibatalkan</span>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row g-5 align-items-center">
                                <div class="col-9">
                                    <div class="row g-3">
                                        @foreach ($items->detail as $item)
                                        <div class="col-12 {{ !$loop->last ? 'pb-3 border-bottom' : '' }}">
                                            <div class="d-flex gap-3">
                                                <img src="{{ url('storage/product', $item->product->galleries[0]->image) }}"
                                                    alt="" class="img-thumbnail img-fluid" width="80">

                                                <div>
                                                    <p class="m-0 fw-bold">{{ $item->product->name }}</p>
                                                    <small class="text-black-50">{{ $item->qty }} barang x {{
                                                        number_format($item->price) }}</small>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col text-center border-start">
                                    <p class="mb-1 text-black-50">Total Harga</p>
                                    <h6 class="m-0">Rp {{ number_format($items->total_harga) }}</h6>
                                </div>
                            </div>
                            @if ($items->status == 2)
                            <hr class="mt-4">
                            <div class="d-flex justify-content-end">
                                <form action="{{ route('riwayat-pesanan.update', $items->id) }}" method="post">
                                    @csrf
                                    @method('put')

                                    <button type="submit" class="btn btn-outline-primary">Pesanan Diterima</button>
                                </form>
                            </div>
                            @endif

                            @if ($items->status == 3 && !$items->is_review)
                            <hr class="mt-4">
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal"
                                    data-bs-target="#review-{{ $items->id }}">
                                    Beri Ulasan
                                </button>

                                <div class="modal fade" id="review-{{ $items->id }}" tabindex="-1"
                                    aria-labelledby="review-{{ $items->id }}Label" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="review-{{ $items->id }}Label">Ulasan
                                                </h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form action="{{ route('review.store') }}" method="post">
                                                @csrf

                                                <div class="modal-body">
                                                    <div class="row">
                                                        @foreach ($items->detail as $item)
                                                        <div class="col-12 g-3">
                                                            <div class="card shadow-sm">
                                                                <div class="card-body">
                                                                    <h6 class="mb-1">{{ $item->product->name }}</h6>
                                                                    <input type="hidden" name="transaction_id"
                                                                        value="{{ $items->id }}">
                                                                    <input type="hidden" name="product_id[]"
                                                                        value="{{ $item->product_id }}">
                                                                    <div class="d-flex justify-content-start">
                                                                        <div class="stars">
                                                                            <input type="radio"
                                                                                name="rating[{{ $loop->index }}]"
                                                                                id="rate-5-{{ $item->id }}" value="5"
                                                                                required>
                                                                            <label for="rate-5-{{ $item->id }}"></label>
                                                                            <input type="radio"
                                                                                name="rating[{{ $loop->index }}]"
                                                                                id="rate-4-{{ $item->id }}" value="4"
                                                                                required>
                                                                            <label for="rate-4-{{ $item->id }}"></label>
                                                                            <input type="radio"
                                                                                name="rating[{{ $loop->index }}]"
                                                                                id="rate-3-{{ $item->id }}" value="3"
                                                                                required>
                                                                            <label for="rate-3-{{ $item->id }}"></label>
                                                                            <input type="radio"
                                                                                name="rating[{{ $loop->index }}]"
                                                                                id="rate-2-{{ $item->id }}" value="2"
                                                                                required>
                                                                            <label for="rate-2-{{ $item->id }}"></label>
                                                                            <input type="radio"
                                                                                name="rating[{{ $loop->index }}]"
                                                                                id="rate-1-{{ $item->id }}" value="1"
                                                                                required>
                                                                            <label for="rate-1-{{ $item->id }}"></label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="mt-2">
                                                                        <textarea name="ulasan[]" id="ulasan" rows="2"
                                                                            class="form-control"
                                                                            placeholder="Tulisan ulasan kamu disini..."
                                                                            style="resize: none"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Kirim</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    @empty
                    <p class="m-0 text-center text-secondary opacity-50 fs-4 py-4">Belum ada riwayat pesanan</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<style>
    .stars {
        /* float: left; */
        /* overflow: hidden; */
        /* width: 100%; */
        /* display: flex;
        justify-content: center; */
    }

    .stars input[type=radio]:checked~label:after {
        color: gold;
    }

    .stars input[type=radio] {
        display: none;
    }

    .stars input[type=radio]:first-child+label {
        padding-right: 0;
    }

    .stars:hover input[type=radio]:checked~label:after,
    .stars label:after {
        color: gray;
    }

    .stars label {
        box-sizing: border-box;
        /* width: 20%; */
        /* padding-right: 2%; */
        /* height: 120px; */
        float: right;
        cursor: pointer;
    }

    .stars label:after {
        display: block;
        content: "★";
        font-size: 25px;
        color: gray;
        /* height: 120px;
        width: 100%; */
        /* float: right; */
        transition: all 0.25s;
        /* background: gray; */
    }

    .stars label:hover:after,
    .stars label:hover~label:after {
        color: gold !important;
    }
</style>
@endpush