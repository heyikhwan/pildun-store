@extends('layouts.app')

@section('title', 'Nama Produk')

@section('content')
<section>
    <form action="{{ route('keranjang.store', $product->id) }}" method="post">
        @csrf

        <div class="row">
            <div class="col-12 col-lg-5 rounded overflow-hidden">
                <div id="fotoProduk" class="carousel slide" data-bs-ride="true">
                    <div class="carousel-indicators">
                        @foreach ($product->galleries as $gallery)
                        <button type="button" data-bs-target="#fotoProduk" data-bs-slide-to="0" @if($loop->first)
                            class="active"
                            aria-current="true" @endif aria-label="Slide {{ $loop->index + 1 }}"></button>
                        @endforeach
                    </div>
                    <div class="carousel-inner">
                        @foreach ($product->galleries as $gallery)
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                            <img src="{{ url('storage/product', $gallery->image) }}" class="d-block w-100"
                                alt="{{ $product->name }}">
                        </div>
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#fotoProduk"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#fotoProduk"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>

            <div class="col-12 col-lg-7">
                <div class="card rounded border-0 shadow-sm">
                    <div class="card-body">
                        <p class="fw-bold fs-5 mb-1">{{ $product->name }}</p>
                        <div class="mb-3 d-flex align-items-center">
                            @if ($product->stock > 0)
                            <span class="badge bg-success">Stok Tersedia</span>
                            @else
                            <span class="badge bg-danger">Stok Habis</span>
                            @endif
                            <span class="text-black-50 mx-2">|</span>
                            <span class="text-black-50">Stok {{ $product->stock }}</span>
                            <span class="text-black-50 mx-2">|</span>
                            <span class="text-black-50"><i class="ri-star-fill text-warning"></i> {{ $rating }}</span>
                        </div>
                        <h5 class="fs-4 fw-bold">Rp {{ number_format($product->price) }}</h5>
                        <hr class="my-4">
                        <table class="table table-borderless" style="width: 50%">
                            <tbody>
                                <tr>
                                    <td class="text-black-50">Berat Satuan</td>
                                    <td>{{ $product->weight }} g</td>
                                </tr>
                                <tr>
                                    <td class="text-black-50">Kategori</td>
                                    <td><a href="{{ route('kategori.show', $product->category->slug) }}"
                                            class="text-decoration-none">{{ $product->category->name }}</a></td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="my-4">
                        <label for="qty" class="form-label">Quantity</label>
                        <input type="number" name="qty" id="qty" min="1" max="{{ $product->stock }}"
                            value="{{ $product->stock > 0 ? 1 : 0 }}" class="form-control @error('qty') is-invalid @enderror" style="width: 80px"
                            @if($product->stock == 0) disabled @endif>
                        <small class="text-black-50">Max. pembelian {{ $product->stock }} pcs</small>
                        @error('qty')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror

                        <div class="d-grid mt-4">
                            <button type="submit"
                                class="btn btn-primary d-flex align-items-center gap-2 justify-content-center"
                                @if($product->stock == 0) disabled @endif>
                                <i class="ri-shopping-cart-fill"></i>
                                <span>Tambah Keranjang</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<section class="mt-3">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm rounded">
                <div class="card-body">
                    <p class="fw-bold">Deskripsi Produk</p>
                    <p>{{ $product->description }}</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mt-3">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm rounded">
                <div class="card-body">
                    <div class="d-flex align-items-center gap-3 mb-3">
                        <span class="fw-bold">Ulasan ({{ $reviews->count() }})</span>
                        <span><i class="ri-star-fill text-warning"></i> {{ $rating }}</span>
                    </div>

                    <div class="row g-3">
                        @foreach ($reviews as $item)
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-column gap-0">
                                        <small><b>{{ $item->user->name }}</b></small>
                                        <span>
                                            @for ($i = 0; $i < $item->rating; $i++)
                                                <i class="ri-star-fill text-warning"></i>
                                            @endfor
                                        </span>
                                        <small class="text-black-50">{{ $item->created_at->format('d F Y') }}</small>
                                    </div>
                                    <p class="mt-2 mb-0">{{ $item->ulasan }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@if (!$related_products->isEmpty())
<section class="mt-5">
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h5>Pilihan Lainnya Untuk Kamu</h5>
        <a href="#" class="text-decoration-none text-primary">Lihat Semua</a>
    </div>

    <div class="row g-3">
        @foreach ($related_products as $item)
        <div class="col-6 col-lg-2">
            @include('frontend.partials.card-product')
        </div>
        @endforeach
    </div>
</section>
@endif
@endsection