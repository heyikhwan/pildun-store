@extends('layouts.app')

@section('title', $title)

@section('content')
<section>
    <div class="card border-0 shadow-sm">
        <div class="card-body">
            <div class="d-flex align-items-center justify-content-between">
                <div>
                    <h5 class="m-0">{{ $title }}</h5>
                    <small class="text-secondary">{{ $description }}</small>
                </div>

                @include('frontend.partials.search')
            </div>

            <div class="row g-4 my-2">
                @forelse ($products as $item)
                <div class="col-6 col-lg-3">
                    @include('frontend.partials.card-product')
                </div>
                @empty
                <div class="col-12">
                    <p class="m-0 text-center text-secondary opacity-50 fs-4 py-4">Produk tidak tersedia</p>
                </div>
                @endforelse
            </div>

            <div class="mt-3">
                {{ $items->links() }}
            </div>
        </div>
    </div>
</section>
@endsection