@extends('layouts.app')

@section('title', 'Pildun Store')

@section('content')
<!-- Banner -->
@if (!$banners->isEmpty())
<div id="banner" class="carousel slide rounded overflow-hidden" data-bs-ride="true">
    <div class="carousel-indicators">
        @foreach ($banners as $item)
        <button type="button" data-bs-target="#banner" data-bs-slide-to="0" @if($loop->first)
            class="active" aria-current="true" @endif aria-label="Slide {{ $loop->index + 1 }}"></button>
        @endforeach
    </div>
    <div class="carousel-inner">
        @foreach ($banners as $item)
        <div data-bs-interval="2000" class="carousel-item {{ $loop->first ? 'active' : '' }}">
            <img src="{{ url('storage/banner', $item->image) }}" class="d-block w-100">
        </div>
        @endforeach
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#banner" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#banner" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
@endif

<!-- Kategori -->
<section class="card border-0 py-3 shadow-sm my-4">
    <div class="card-body">
        <div class="row g-3 px-lg-5 justify-content-arround">
            @forelse ($categories as $item)
            <div class="col-4 col-lg-2 text-center">
                <a href="{{ route('kategori.show', $item->slug) }}" class="text-decoration-none text-dark">
                    <div class="card rounded mb-1">
                        <div class="card-body text-center">
                            <img src="{{ url('storage/kategori', $item->icon) }}" alt="{{ $item->name }}"
                                class="img-fluid" width="50" height="50">
                        </div>
                    </div>
                    <span>{{ $item->name }}</span>
                </a>
            </div>
            @empty
            <div class="col-12">
                <p class="m-0 text-center text-secondary opacity-50 fs-4 py-4">Kategori tidak tersedia.</p>
            </div>
            @endforelse
        </div>
    </div>
</section>

<!-- Produk Terbaru -->
<section>
    <div class="card border-0 shadow-sm">
        <div class="card-body">
            <div class="d-flex align-items-center justify-content-between">
                <div>
                    <h5 class="m-0">Produk Terbaru</h5>
                    <small class="text-secondary">Temukan berbagai macam produk kesukaan kamu</small>
                </div>

                @include('frontend.partials.search')
            </div>

            <div class="row g-4 my-2">
                @forelse ($products as $item)
                <div class="col-6 col-lg-3">
                    @include('frontend.partials.card-product')
                </div>
                @empty
                <div class="col-12">
                    <p class="m-0 text-center text-secondary opacity-50 fs-4 py-4">Produk tidak tersedia</p>
                </div>
                @endforelse
            </div>

            @if (!$products->isEmpty())
            <div class="d-flex justify-content-center mt-5 mb-3">
                <a href="{{ route('produk.index') }}"
                    class="text-decoration-none d-flex align-items-center gap-1 btn btn-outline-primary">
                    <span>Lihat Semua Produk</span> <i class="ri-arrow-right-fill"></i>
                </a>
            </div>
            @endif
        </div>
    </div>
</section>
@endsection