<?php

namespace App\Providers;

use App\Models\Transaksi;
use App\Models\TransaksiDetail;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapFive();
        View::composer(
            [
                'layouts.app',
            ],
            function ($view) {
                if (Auth::check()) {
                    $transaction = Transaksi::where('user_id', auth()->user()->id)->where('status', 0)->first();
                    if (!is_null($transaction)) {
                        $view->with('cart_count', TransaksiDetail::where('transaksi_id', $transaction->id)->count());
                    } else {
                        $view->with('cart_count', 0);
                    }
                }
            }
        );
    }
}
