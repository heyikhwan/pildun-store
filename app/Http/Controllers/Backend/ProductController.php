<?php

namespace App\Http\Controllers\Backend;

use App\Models\Ulasan;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\ProductGallery;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('galleries')->orderBy('name')->get();
        $reviews = Review::with(['product', 'user'])->orderBy('created_at', 'desc')->limit(5)->get();

        return view('backend.pages.produk.index', [
            'products' => $products,
            'reviews' => $reviews,
        ]);
    }

    public function data()
    {
        $data = Product::orderBy('name')->get();

        return DataTables::of($data)
            ->addColumn('category', function ($data) {
                return $data->category->name;
            })
            ->addColumn('aksi', function ($data) {
                $button = '<div class="dropdown d-inline-block"><button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="ri-more-fill align-middle"></i></button><ul class="dropdown-menu dropdown-menu-end"><li><button data-bs-toggle="modal" data-bs-target="#modalFoto-' . $data->id . '" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i>Lihat Foto</button></li><li><a href="' . route('admin.produk.edit', $data->id) . '" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li><li><a class="dropdown-item remove-item-btn" onclick="confirmDelete(' . $data->id . ')">
                <form action="' . route('admin.produk.destroy', $data->id) . '" method="POST" id="delete-' . $data->id . '"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="delete" /></form><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Hapus</a></li></ul></div>';
                return $button;
            })
            ->rawColumns(['category', 'aksi'])
            ->make(true);
    }


    public function create()
    {
        $categories = Category::orderBy('name')->get();

        return view('backend.pages.produk.create', [
            'categories' => $categories
        ]);
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'category' => 'required',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'weight' => 'required|integer',
            'description' => 'required|string',
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.integer' => ':attribute harus berupa angka.',
        ], [
            'name' => 'nama produk',
            'category' => 'kategori',
            'price' => 'harga',
            'stock' => 'stok',
            'weight' => 'berat',
            'description' => 'deskripsi',
        ]);

        $slug = \Str::slug($request->name) . '-' . rand(100, 999);

        Product::create([
            'name' => $data['name'],
            'slug' => $slug,
            'category_id' => $data['category'],
            'price' => $data['price'],
            'weight' => $data['weight'],
            'stock' => $data['stock'],
            'description' => $data['description'],
        ]);

        $product = Product::latest()->first();

        // Store Foto Produk
        if (!is_null($request->foto[0])) {
            foreach ($request->foto as $item) {
                $foto_decode = json_decode($item);
                $path = rand() . '-' . str_replace(' ', '-', $foto_decode->name);
                $file_foto = base64_decode($foto_decode->data);
                Storage::put('public/product/' . $path, $file_foto);

                ProductGallery::create([
                    'product_id' => $product->id,
                    'name' => $foto_decode->name,
                    'image' => $path
                ]);
            }
        }

        return to_route('admin.produk.index')->with('success', 'Produk berhasil ditambahkan');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::orderBy('name')->get();

        return view('backend.pages.produk.edit', [
            'product' => $product,
            'categories' => $categories,
        ]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $data = $request->validate([
            'name' => 'required|string',
            'category' => 'required',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'weight' => 'required|integer',
            'description' => 'required|string',
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.integer' => ':attribute harus berupa angka.',
        ], [
            'name' => 'nama produk',
            'category' => 'kategori',
            'price' => 'harga',
            'stock' => 'stok',
            'weight' => 'berat',
            'description' => 'deskripsi',
        ]);

        if ($product->name !== $request->name) {
            $slug = \Str::slug($request->name) . '-' . rand(100, 999);
        } else {
            $slug = $product->slug;
        }

        $product->update([
            'name' => $data['name'],
            'slug' => $slug,
            'category_id' => $data['category'],
            'price' => $data['price'],
            'stock' => $data['stock'],
            'weight' => $data['weight'],
            'description' => $data['description'],
        ]);

        // Store Foto
        if (!is_null($request->foto[0])) {
            foreach ($request->foto as $item) {
                $foto_decode = json_decode($item);
                $path = rand() . '-' . str_replace(' ', '-', $foto_decode->name);
                $file_foto = base64_decode($foto_decode->data);
                if (!is_null($product->foto)) {
                    if (file_exists(public_path('storage/product/') . $product->foto)) {
                        unlink(public_path('storage/product/') . $product->foto);
                    }
                }
                Storage::put('public/product/' . $path, $file_foto);

                ProductGallery::create([
                    'product_id' => $id,
                    'name' => $foto_decode->name,
                    'image' => $path
                ]);
            }
        }

        return to_route('admin.produk.index')->with('success', 'Produk berhasil diperbarui');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $photos = $product->galleries;

        foreach ($photos as $photo) {

            if (file_exists(public_path('storage/product/') . $photo->image)) {
                unlink(public_path('storage/product/') . $photo->image);
            }

            $photo->delete();
        }

        $product->delete();

        return to_route('admin.produk.index')->with('success', 'Produk berhasil dihapus');
    }

    public function imgDestroy($id)
    {
        $data = ProductGallery::findOrFail($id);

        if (file_exists(public_path('storage/product/') . $data->image)) {
            unlink(public_path('storage/product/') . $data->image);
        }

        $data->delete();

        return response()->json(['success' => 'Foto berhasil dihapus']);
    }
}
