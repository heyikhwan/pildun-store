<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BannerController extends Controller
{
    public function index()
    {
        return view('backend.pages.banner.index');
    }

    public function data()
    {
        $data = Banner::latest()->get();

        return DataTables::of($data)
            ->addColumn('image', function ($data) {
                $img = '<a href="' . url('storage/banner', $data->image) . '" data-fancybox="' . $data->name . '" data-caption="' . $data->name . '"><img src="' . url('storage/banner', $data->image) . '" alt="' . $data->name . '" width="45" height="45" class="img-fluid img-thumbnail"></a>';

                return $img;
            })
            ->addColumn('aksi', function ($data) {
                $button = '<a href="' . route('admin.banner.edit', $data->id) . '" class="btn btn-sm btn-soft-info me-1">Edit</a>';

                $button .= '<a href="#" class="btn btn-sm btn-soft-danger" onclick="confirmDelete(' . $data->id . ')">
            <form action="' . route('admin.banner.destroy', $data->id) . '" method="POST"
                id="delete-' . $data->id . '"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="delete" /></form>Hapus</a>';

                return $button;
            })
            ->rawColumns(['image', 'aksi'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.pages.banner.create');
    }

    public function store(Request $request)
    {
        $path = null;

        if (!is_null($request->image)) {
            $image_decode = json_decode($request->image);
            $path = rand() . '-' . str_replace(' ', '-', $image_decode->name);
            $file_image = base64_decode($image_decode->data);

            \Storage::put('public/banner/' . $path, $file_image);
        }

        Banner::create([
            'image' => $path,
        ]);

        return to_route('admin.banner.index')->with('success', 'Banner berhasil ditambahkan');
    }

    public function edit($id)
    {
        $item = Banner::findOrFail($id);

        return view('backend.pages.banner.edit', [
            'item' => $item
        ]);
    }

    public function update(Request $request, $id)
    {
        $item = Banner::findOrFail($id);

        if (!is_null($request->image)) {
            $image_decode = json_decode($request->image);
            $path = rand() . '-' . str_replace(' ', '-', $image_decode->name);
            $file_image = base64_decode($image_decode->data);

            \Storage::put('public/banner/' . $path, $file_image);

            if ($item->image) {
                if (file_exists(public_path('storage/banner/') . $item->image)) {
                    unlink(public_path('storage/banner/') . $item->image);
                }
            }
        } else {
            $path = $item->image;
        }

        $item->update([
            'image' => $path,
        ]);

        return to_route('admin.banner.index')->with('success', 'Banner berhasil diperbarui');
    }

    public function destroy($id)
    {
        $item = Banner::findOrFail($id);

        if ($item->image) {
            if (file_exists(public_path('storage/banner/') . $item->image)) {
                unlink(public_path('storage/banner/') . $item->image);
            }
        }

        $item->delete();

        return to_route('admin.banner.index')->with('success', 'banner berhasil dihapus');
    }
}
