<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReviewController extends Controller
{
    public function index()
    {
        return view('backend.pages.ulasan.index');
    }

    public function data()
    {
        $data = Review::with(['user', 'product'])->latest()->get();

        return DataTables::of($data)
            ->addColumn('user', function ($data) {
                return $data->user->name;
            })
            ->addColumn('product', function ($data) {
                return $data->product->name;
            })
            ->addColumn('rating', function ($data) {
                $rating = '<i class="ri-star-fill text-warning"></i> ' . $data->rating;
                return $rating;
            })
            ->addColumn('aksi', function ($data) {
                $button = '<a href="#" class="btn btn-sm btn-soft-danger" onclick="confirmDelete(' . $data->id . ')">
            <form action="' . route('admin.ulasan.destroy', $data->id) . '" method="POST"
                id="delete-' . $data->id . '"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="delete" /></form>Hapus</a>';

                return $button;
            })
            ->rawColumns(['user', 'product', 'rating', 'aksi'])
            ->make(true);
    }

    public function destroy($id)
    {
        $item = Review::findOrFail($id);

        $item->delete();

        return to_route('admin.ulasan.index')->with('success', 'Ulasan berhasil dihapus');
    }
}
