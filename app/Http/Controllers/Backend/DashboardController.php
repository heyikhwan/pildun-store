<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Product;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use App\Models\TransaksiDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        // Total Pendapatan
        $pendapatan = Transaksi::where('status', 3)->sum('total_harga');
        // Pesanan
        $pesanan = Transaksi::where('status', 3)->count();
        // Customers
        $customer = User::role('user')->count();
        // Total produk
        $product = Product::count();
        // 5 pesanan terbaru
        $pesanan_terbaru = Transaksi::orderBy('created_at', 'desc')->limit(5)->get();
         // Bulan
         $bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
         for($i = 0; $i < count($bulan); $i++){
             $bulan[$i];
         }

        // Grafik Keuangan
        // Pendapatan
        $pendapatanPerBulan = Transaksi::whereYear('created_at', 2022)->get()->groupBy(function($data){
            return Carbon::parse($data->created_at)->format('m');
        });

        $pendapatan2=[];
        $pendapatan2Arr=[];
        foreach($pendapatanPerBulan as $month => $values) {   
            foreach($values as $value) {
                $pendapatan2Arr[$month-1]=$value->sum('total_harga');
            }
        }

        for ($i = 0; $i <= 11; $i++) {
            if(!empty($pendapatan2Arr[$i])){
                $pendapatan2[$i] = $pendapatan2Arr[$i];    
            }else{
                $pendapatan2[$i] = 0;    
            }
        }
        // dd($pendapatan2);

        return view('backend.pages.dashboard.index', [
            'pendapatan' => $pendapatan,
            'pesanan' => $pesanan,
            'customer' => $customer,
            'product' => $product,
            'pendapatan2' => $pendapatan2,
            'bulan' => $bulan,
            'pesanan_terbaru' => $pesanan_terbaru,
        ]);
    }
}
