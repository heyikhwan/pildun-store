<?php

namespace App\Http\Controllers\Backend;

use App\Models\Transaksi;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;

class TransaksiController extends Controller
{
    public function index()
    {
        $transaksi = Transaksi::with('detail')->orderBy('created_at')->get();
        return view('backend.pages.transaksi.index', [
            'transaksi' => $transaksi
        ]);
    }

    public function data()
    {
        $data = Transaksi::get();

        return DataTables::of($data)
        ->addColumn('user', function ($data) {
            return $data->user->name;
        })
        ->addColumn('status', function ($data) {
            if ($data->status == 1) {
                $status = '<span class="badge bg-info">Sedang diproses</span>';
            } elseif ($data->status == 2) {
                $status = '<span class="badge bg-warning">Dikirim</span>';
            } elseif ($data->status == 3) {
                $status = '<span class="badge bg-success">Diterima</span>';
            } elseif ($data->status == 4) {
                $status = '<span class="badge bg-danger">Dibatalkan</span>';
            }
            
            return $status;
        })
        ->addColumn('created_at', function ($data) {
            return '<div>'.$data->created_at->format('d F Y').'</div>
            <div>'.$data->created_at->format('H:i'). ' WIB'.'</div>';
        })
            ->addColumn('aksi', function ($data) {
                $button = '<div class="d-grid gap-1">';
                $button .= '<button data-bs-toggle="modal" data-bs-target="#modalDetail-'.$data->id.'" class="btn btn-sm btn-soft-info mb-1">Lihat Detail</button>';
                if($data->status == 1) {
                $button .= '<form action="' . route('admin.transaksi.updateOrder', $data->id) . '" method="POST"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="put" /> <button type="submit" class="btn btn-sm btn-soft-warning mb-1">Pesanan Dikirim</button></form>';
                } 
                if($data->status != 4 && $data->status != 3) {
                    $button .= '<a href="#" class="btn btn-sm btn-soft-danger" onclick="confirmCancel(' . $data->id . ')">
                    <form action="' . route('admin.transaksi.cancelOrder', $data->id) . '" method="POST"
                        id="cancel-' . $data->id . '"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="put" /></form>Batalkan Pesanan</a>';
                    }
                $button .= '</div>';
                return $button;
            })
            ->rawColumns(['user', 'status', 'created_at', 'aksi'])
            ->make(true);
    }

    public function updateOrder($id)
    {
        $transaksi = Transaksi::findOrFail($id);

        $transaksi->update([
            'status' => 2,
        ]);

        return to_route('admin.transaksi.index')->with('success', 'Pesanan berhasil diupdate');
    }

    public function completeOrder($id)
    {
        $transaksi = Transaksi::findOrFail($id);

        $transaksi->update([
            'status' => 3,
        ]);

        return to_route('admin.transaksi.index')->with('success', 'Pesanan selesai');
    }

    public function cancelOrder($id)
    {
        $transaksi = Transaksi::findOrFail($id);

        $transaksi->update([
            'status' => 4,
        ]);

        return to_route('admin.transaksi.index')->with('success', 'Pesanan berhasil dibatalkan');
    }

}
