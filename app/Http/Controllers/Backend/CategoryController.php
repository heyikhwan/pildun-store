<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    public function index()
    {
        return view('backend.pages.kategori.index');
    }

    public function data()
    {
        $data = Category::latest()->get();

        return DataTables::of($data)
            ->addColumn('icon', function ($data) {
                $img = '<a href="' . url('storage/kategori', $data->icon) . '" data-fancybox="' . $data->name . '" data-caption="' . $data->name . '"><img src="' . url('storage/kategori', $data->icon) . '" alt="' . $data->name . '" width="45" height="45" class="img-fluid img-thumbnail"></a>';

                return $img;
            })
            ->addColumn('aksi', function ($data) {
                $button = '<a href="' . route('admin.kategori.edit', $data->id) . '" class="btn btn-sm btn-soft-info me-1">Edit</a>';

                $button .= '<a href="#" class="btn btn-sm btn-soft-danger" onclick="confirmDelete(' . $data->id . ')">
            <form action="' . route('admin.kategori.destroy', $data->id) . '" method="POST"
                id="delete-' . $data->id . '"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="delete" /></form>Hapus</a>';

                return $button;
            })
            ->rawColumns(['icon', 'aksi'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.pages.kategori.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255|unique:categories',
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.max' => 'panjang :attribute tidak boleh lebih dari :max karakter.',
            '*.unique' => ':attribute sudah tersedia.',
        ], [
            'name' => 'nama kategori'
        ]);

        $path = null;
        $slug = \Str::slug($request->name);

        if (!is_null($request->icon)) {
            $icon_decode = json_decode($request->icon);
            $path = rand() . '-' . str_replace(' ', '-', $icon_decode->name);
            $file_icon = base64_decode($icon_decode->data);

            \Storage::put('public/kategori/' . $path, $file_icon);
        }

        Category::create([
            'name' => $data['name'],
            'slug' => $slug,
            'icon' => $path,
        ]);

        return to_route('admin.kategori.index')->with('success', 'Kategori berhasil ditambahkan');
    }

    public function edit($id)
    {
        $item = Category::findOrFail($id);

        return view('backend.pages.kategori.edit', [
            'item' => $item
        ]);
    }

    public function update(Request $request, $id)
    {
        $item = Category::findOrFail($id);

        $data = $request->validate([
            'name' => 'required|string|max:255|unique:categories,name,' . $item->id,
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.max' => 'panjang :attribute tidak boleh lebih dari :max karakter.',
            '*.unique' => ':attribute sudah tersedia.',
        ], [
            'name' => 'nama kategori'
        ]);

        if ($item->name !== $request->name) {
            $slug = \Str::slug($request->name);
        } else {
            $slug = $item->slug;
        }

        if (!is_null($request->icon)) {
            $icon_decode = json_decode($request->icon);
            $path = rand() . '-' . str_replace(' ', '-', $icon_decode->name);
            $file_icon = base64_decode($icon_decode->data);

            \Storage::put('public/kategori/' . $path, $file_icon);

            if ($item->icon) {
                if (file_exists(public_path('storage/kategori/') . $item->icon)) {
                    unlink(public_path('storage/kategori/') . $item->icon);
                }
            }
        } else {
            $path = $item->icon;
        }

        $item->update([
            'name' => $request->name,
            'slug' => $slug,
            'icon' => $path,
        ]);

        return to_route('admin.kategori.index')->with('success', 'Kategori berhasil diperbarui');
    }

    public function destroy($id)
    {
        $item = Category::findOrFail($id);

        if ($item->icon) {
            if (file_exists(public_path('storage/kategori/') . $item->icon)) {
                unlink(public_path('storage/kategori/') . $item->icon);
            }
        }

        $item->delete();

        return to_route('admin.kategori.index')->with('success', 'Kategori berhasil dihapus');
    }
}
