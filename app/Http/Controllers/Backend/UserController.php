<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        return view('backend.pages.user.index');
    }

    public function data()
    {
        $data = User::orderBy('name')->get();

        return DataTables::of($data)
            ->addColumn('roles', function ($data) {
                if ($data->getRoleNames()[0] === 'admin') {
                    $td = '<span class="badge badge-gradient-primary ">' . $data->getRoleNames()[0] . '</span>';
                } else {
                    $td = ' <span class="badge badge-gradient-danger ">' . $data->getRoleNames()[0] . '</span>';
                }
                return $td;
            })
            ->addColumn('aksi', function ($data) {
                $button = '<a href="' . route('admin.user.edit', $data->id) . '" class="btn btn-sm btn-soft-info me-1">Edit</a>';

                $button .= '<a href="#" class="btn btn-sm btn-soft-danger" onclick="confirmDelete(' . $data->id . ')">
            <form action="' . route('admin.user.destroy', $data->id) . '" method="POST"
                id="delete-' . $data->id . '"><input type="hidden" name="_token" value="' . csrf_token() . '" /><input type="hidden" name="_method" value="delete" /></form>Hapus</a>';

                return $button;
            })
            ->rawColumns(['roles', 'aksi'])
            ->make(true);
    }

    public function create()
    {
        $roles = Role::all()->pluck('name');
        return view('backend.pages.user.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required'],
            'username' => ['required', 'unique:users'],
            'email' => ['required', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'no_telp' => ['numeric', 'nullable'],
            'role' => ['required']
        ]);

        User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'no_telp' => $data['no_telp']
        ])->assignRole($data['role']);

        return view('backend.pages.user.index');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all()->pluck('name');
        return view('backend.pages.user.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $data = $request->validate([
            'name' => ['required'],
            'username' => ['required', 'unique:users,username,' . $user->id],
            'email' => ['required', 'unique:users,email,' . $user->id],
            'password' => ['sometimes', 'required', 'string', 'min:8'],
            'no_telp' => ['numeric', 'max_digits: 13'],
            'role' => ['required']
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.max' => 'panjang :attribute tidak boleh lebih dari :max karakter.',
            '*.unique' => ':attribute sudah digunakan.',
            '*.numeric' => ':attribute harus berupa angka.',
            '*.min' => ':attribute minimal :min karakter.',
            '*.max_digits' => ':attribute tidak boleh lebih dari :max digit.',
        ], [
            'name' => 'nama',
            'no_telp' => 'no. handphone',
        ]);

        $user->update([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'no_telp' => $data['no_telp']
        ]);

        $user->syncRoles($data['role']);

        return view('backend.pages.user.index');
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        return view('backend.pages.user.index');
    }
}
