<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index()
    {
        $histories = Transaksi::with('detail')->where('user_id', auth()->user()->id)->where('status', '>', 0)->latest()->get();

        return view('frontend.pages.riwayat.index', [
            'histories' => $histories
        ]);
    }

    public function update($id)
    {
        $transaction = Transaksi::findOrFail($id);

        $transaction->update([
            'status' => 3
        ]);

        return redirect()->back();
    }

    public function review(Request $request)
    {
        for ($i=0; $i < count($request->product_id); $i++) { 
            Review::create([
                'user_id' => auth()->user()->id,
                'product_id' => $request->product_id[$i],
                'rating' => $request->rating[$i],
                'ulasan' => $request->ulasan[$i],
            ]);
        }

        $transaction = Transaksi::findOrFail($request->transaction_id);
        $transaction->update([
            'is_review' => 1
        ]);

        return redirect()->back();
    }
}
