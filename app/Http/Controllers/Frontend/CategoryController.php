<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->first();
        $products = Product::where('category_id', $category->id)->paginate(16);

        return view('frontend.pages.list-produk.index', [
            'title' => 'Kategori: ' . $category->name,
            'description' => 'Temukan produk impian kamu dari kategori ' . $category->name,
            'products' => $products
        ]);
    }
}
