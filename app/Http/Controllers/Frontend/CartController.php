<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaksi;
use App\Models\TransaksiDetail;
use App\Models\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function cart(Request $request, $id)
    {
        $transaction = Transaksi::where('user_id', auth()->user()->id)->where('status', 0)->first();
        $product = Product::findOrFail($id);

        $request->validate([
            'qty' => 'integer|max:' . $product->stock
        ], [
            '*.integer' => ':attribute harus berupa angka.',
            '*.max' => 'maksimal pembelian adalah ' . $product->stock . ' pcs.'
        ], [
            'qty' => 'quantity'
        ]);

        if (!$transaction) {
            $transaction = Transaksi::create([
                'user_id' => auth()->user()->id,
                'status' => 0,
                'total_harga' => 0
            ]);
        }

        $transaction_detail = TransaksiDetail::where('transaksi_id', $transaction->id)->where('product_id', $product->id)->first();

        if ($transaction_detail) {
            $qty = $request->qty + $transaction_detail->qty;
            $sub_total = $transaction_detail->sub_total + ($product->price * $request->qty);

            $transaction_detail->update([
                'qty' => $qty,
                'sub_total' => $sub_total
            ]);
        } else {
            $sub_total = $product->price * $request->qty;

            TransaksiDetail::create([
                'product_id' => $id,
                'transaksi_id' => $transaction->id,
                'qty' => $request->qty,
                'sub_total' =>  $sub_total,
                'price' => $product->price,
            ]);

            $transaction->update([
                'total_harga' => $transaction->total_harga + ($product->price * $request->qty)
            ]);
        }

        return redirect()->back();
    }

    public function index()
    {
        $transaction = Transaksi::with('detail')->where('user_id', auth()->user()->id)->where('status', 0)->first();

        return view('frontend.pages.keranjang.index', [
            'transaction' => $transaction
        ]);
    }

    public function update(Request $request, $id)
    {
        $transaction = Transaksi::with('detail')->findOrFail($id);

        foreach ($transaction->detail as $index => $item) {
            if ($item->qty != $request->qty[$index]) {
                $sub_total = $request->qty[$index] * $item->product->price;

                $transaction->update([
                    'total_harga' => $transaction->total_harga - ($item->qty * $item->product->price)
                ]);

                $item->update([
                    'qty' => $request->qty[$index],
                    'sub_total' => $sub_total,
                    'price' => $item->product->price,
                ]);

                $transaction->update([
                    'total_harga' => $transaction->total_harga + $sub_total
                ]);
            }
        }

        return to_route('keranjang.checkout');
    }

    public function checkout()
    {
        $user = User::findOrFail(auth()->user()->id);
        $transaction = Transaksi::with('detail')->where('user_id', auth()->user()->id)->where('status', 0)->first();

        return view('frontend.pages.keranjang.checkout', [
            'user' => $user,
            'transaction' => $transaction,
        ]);
    }

    public function payment(Request $request, $id)
    {
        $transaction = Transaksi::with('detail')->findOrFail($id);

        $request->validate([
            'name' => 'required|string|max:255',
            'no_telp' => 'numeric|max_digits:13|nullable',
            'address' => 'required|string|max:255',
            'note' => 'string|max:255|nullable',
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.max' => 'panjang :attribute tidak boleh lebih dari :max karakter.',
            '*.numeric' => ':attribute harus berupa angka.',
            '*.max_digits' => ':attribute tidak boleh lebih dari :max digit.',
        ], [
            'name' => 'nama',
            'no_telp' => 'no. handphone',
            'address' => 'alamat',
            'note' => 'catatan',
        ]);

        $invoice = 'INV/' . rand(100, 99999);

        $transaction->update([
            'no_invoice' => $invoice,
            'status' => 1,
            'note' => $request->note
        ]);

        foreach ($transaction->detail as $item) {
            $product = Product::findOrFail($item->product->id);
            $stock = $item->product->stock - $item->qty;

            $product->update([
                'stock' => $stock
            ]);
        }

        $user = User::findOrFail(auth()->user()->id);

        $user->update([
            'name' => $request->name,
            'no_telp' => $request->no_telp,
            'address' => $request->address,
        ]);

        return to_route('payment.success');
    }

    public function delete($id)
    {
        $transaction_detail = TransaksiDetail::findOrFail($id);
        $transaction = Transaksi::with('detail')->findOrFail($transaction_detail->transaksi_id);

        $transaction->update([
            'total_harga' => $transaction->total_harga - ($transaction_detail->qty * $transaction_detail->price)
        ]);

        $transaction_detail->delete();

        if ($transaction->detail->count()) {
            $transaction->delete();
        }

        return response()->json();
    }
}
