<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Review;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->paginate(16);

        return view('frontend.pages.list-produk.index', [
            'title' => 'Semua Produk',
            'description' => 'Temukan produk impian kamu',
            'products' => $products
        ]);
    }

    public function show($slug)
    {
        $product = Product::with(['category', 'galleries'])->where('slug', $slug)->first();
        $related_products = Product::where('category_id', $product->category->id)->where('id', '<>', $product->id)->Limit(6)->get();

        $reviews = Review::with('user')->where('product_id', $product->id)->latest()->get();
        
        if (!$reviews->isEmpty()) {
            $rating = $reviews->sum('rating') / $reviews->count();
        } else {
            $rating = 0;
        }

        return view('frontend.pages.list-produk.show', [
            'product' => $product,
            'related_products' => $related_products,
            'reviews' => $reviews,
            'rating' => $rating,
        ]);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        
        $products = Product::with('galleries')->where('name', 'like', "%" . $search . "%")->paginate(12);

        return view('frontend.pages.list-produk.index', [
            'title' => 'Cari: ' . $search,
            'description' => 'Hasil pencarian dari produk ' . $search,
            'products' => $products
        ]);
    }
}
