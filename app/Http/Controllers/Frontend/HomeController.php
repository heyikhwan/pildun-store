<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::latest()->get();
        $categories = Category::orderBy('name')->get();
        $products = Product::latest()->limit(16)->get();

        return view('frontend.pages.home.index', [
            'banners' => $banners,
            'categories' => $categories,
            'products' => $products
        ]);
    }
}
