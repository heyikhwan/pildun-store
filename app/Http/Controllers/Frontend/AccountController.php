<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return view('frontend.pages.akun.index', [
                'user' => auth()->user()
            ]);
        } else {
            return redirect()->route('login');
        }
    }

    public function update(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);

        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username,' . $user->id,
            'email' => 'required|string|max:255|unique:users,email,' . $user->id,
            'no_telp' => 'numeric|max_digits:13|nullable',
            'address' => 'string|max:255|nullable',
            'password' => 'sometimes|min:8|max:255|nullable',
        ], [
            '*.required' => ':attribute harus diisi.',
            '*.string' => ':attribute harus berupa string.',
            '*.max' => 'panjang :attribute tidak boleh lebih dari :max karakter.',
            '*.unique' => ':attribute sudah digunakan.',
            '*.numeric' => ':attribute harus berupa angka.',
            '*.min' => ':attribute minimal :min karakter.',
            '*.max_digits' => ':attribute tidak boleh lebih dari :max digit.',
        ], [
            'name' => 'nama',
            'no_telp' => 'no. handphone',
            'address' => 'alamat',
        ]);

        $user->update([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'address' => $request->address,
            'password' => Hash::make($request->password)
        ]);

        return redirect()->back();
    }
}
