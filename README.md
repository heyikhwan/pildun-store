# Pildun Store
- PHP ^8.1.2
- Laravel ^9.42.2

---

Pildun Store adalah website online store yang berfokus menjual produk merchandise world cup.

**Role**
- Admin
- User

**Backend**
- Dashboard
- Kelola Kategori
- Kelola Produk
- Kelola Transaksi
- Kelola Ulasan
- Kelola Banner
- Kelola Pengguna

**Frontend**
- Register
- Login
- Halaman Home
- Halaman Kategori
- Halaman Produk
- Halaman Detail Produk
- Halaman Keranjang
- Halaman Checkout
- Halaman Riwayat Pembelian
- Halaman Akun / Profile
- Halaman Pencarian Produk

**ERD**
![ERD Pildun Store](https://i.postimg.cc/XYqyNhzB/ERD.png)

Video Demo: [Video](https://drive.google.com/file/d/1N1VZEo2DiG7iJHap3RqLe66mSe-9L8Vf/view?usp=share_link)